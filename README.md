# BIOSTAT 823 



## Test GPU settings for torch on B&B GPU node
```
srun --account biostat --partition biostat-gpu --gres=gpu \
     apptainer exec  \
     --nv \
     --bind /work/owzar001/bios-823-container-gpu/examples:/mnt/code/:ro \
     /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
     /opt/pytorch/bin/python3 /mnt/code/checktorch.py 
```

## Test GPU settings for tensorflow on B&B GPU node
```
srun --account biostat --partition biostat-gpu --gres=gpu \
     apptainer exec  \
     --nv \
     --bind /work/owzar001/bios-823-container-gpu/examples:/mnt/code/:ro \
     /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
     /opt/tensorflow/bin/python3 /mnt/code/checktf.py 
```

## Train and test MNIST to example using tensorflow on B&B GPU node
```
srun --account biostat --partition biostat-gpu --gres=gpu --mem 32GB \
     apptainer exec  \
     --nv \
     --bind /work/owzar001/bios-823-container-gpu/examples:/mnt/code/:ro \
     /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
     /opt/tensorflow/bin/python3 /mnt/code/tfmodel1.py
```

## Train pytorch model on B&B GPU node
```
srun --account biostat --partition biostat-gpu --gres=gpu --mem 32GB \
     apptainer exec  \
     --nv \
     --bind /work/owzar001/bios-823-container-gpu/examples:/mnt/code/:ro \
     /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
     /opt/pytorch/bin/python3 /mnt/code/pytorchmodel1.py
```


##  Jupyter on DCC

* From your computer ssh into a DCC node
```
ssh NETID@dcc-login.oit.duke.edu
```


* From the DCC node start a jupyter lab instance. In this example, the
instance is started on a CPU node with 16G of RAM on the DCC common
partion. The notebook is started on port 8889 (the default is 8888).

```
srun --account biostat --partition common --mem=16G \
	singularity exec /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
	/opt/tensorflow/bin/jupyter-lab \
	--no-browser --ip=0.0.0.0 \
	--port=8889
```

* From the output identify the URLs to the jupyter lab instance. 
They will look similar to two lines below. The two piece of information
you should note is the compute node that the SLURM scheduler has picked
for you ( dcc-core-317 in this case) and port number you have picked
(8889 in this case)
```
Or copy and paste one of these URLs:
        http://dcc-core-317:8889/lab?token=c987d148e48e5eb06ee75a5e52f368e1d0145faa1283f51a
        http://127.0.0.1:8889/lab?token=c987d148e48e5eb06ee75a5e52f368e1d0145faa1283f51a

```

* From your computer, establish an ssh tunnel to DCC. Note that 
the requested port (8889) and the assigned compute node (dcc-core-317)

```
ssh -N -f -L 8889:dcc-core-317:8889 NETID@dcc-login.oit.duke.edu
```

* From your computer, open this link your web browser
```
http://127.0.0.1:8889/lab?token=c987d148e48e5eb06ee75a5e52f368e1d0145faa1283f51a
```

* From the DCC node start a jupyter lab instance with GPU support.
Note that you are requesting a GPU node with 16 GB of RAM on the DCC gpu-common
partition. Also note the addition of the --gres=gpu and --nv flags.
```
srun --account biostat --partition gpu-common --mem=16G --gres=gpu \
	singularity exec --nv /opt/apps/containers/community/owzar001/bios-823-container-gpu.sif \
	/opt/tensorflow/bin/jupyter-lab \
	--no-browser --ip=0.0.0.0 \
	--port=8889
```

## Check GPU utilization

```
watch --interval 2 nvidia-smi \
     --query-gpu=index,gpu_name,memory.total,memory.used,memory.free,temperature.gpu,pstate,utilization.gpu,utilization.memory \
     --format=csv

```
