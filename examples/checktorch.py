import torch

print("torch version: " + torch.__version__)
print("is CUDA available? " + str(torch.cuda.is_available()))
print("CUDA device count = " + str(torch.cuda.device_count()))

if torch.cuda.device_count() > 0:
    print("current CUDA device number")
    gpudev = torch.cuda.current_device()
    print(gpudev)
    print("more information about cuda device")
    print(torch.cuda.device(gpudev))
    print(torch.cuda.get_device_name(gpudev))
