import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability import distributions as tfd
import numpy as np
import pandas as pd



print("tensorflow version: " + tf.__version__)
print("tensorflow probability version: " + tfp.__version__)
print("numpy version: " + np.__version__)
print("pandas version: " + pd.__version__)



# list available devices
print(tf.config.list_physical_devices())
print(tf.test.gpu_device_name())


# From https://www.tensorflow.org/probability/api_docs/python/tfp/math/value_and_gradient

tfd = tfp.distributions
tfm = tfp.math

# Case 1: argless `f`.
x = tf.constant(2.)
print(tfm.value_and_gradient(lambda: tf.math.log(x), x))
# ==> [log(2.), 0.5]

# Case 2: packed arguments.
print(tfm.value_and_gradient(lambda x, y: x * tf.math.log(y), [2., 3.]))
# ==> [2. * np.log(3.), (np.log(3.), 2. / 3)]

# Case 3: default.
print(tfm.value_and_gradient(tf.math.log, [1., 2., 3.], auto_unpack_single_arg=False))
# ==> [(log(1.), log(2.), log(3.)), (1., 0.5, 0.333)]
