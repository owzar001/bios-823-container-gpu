import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability import distributions as tfd
import numpy as np
import pandas as pd



print("tensorflow version: " + tf.__version__)
print("tensorflow probability version: " + tfp.__version__)
print("numpy version: " + np.__version__)
print("pandas version: " + pd.__version__)

# list available devices
print(tf.config.list_physical_devices())
print(tf.test.gpu_device_name())
